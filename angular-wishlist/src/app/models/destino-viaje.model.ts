import { ObjectUnsubscribedError } from 'rxjs';
import {v4 as uuid} from 'uuid';

export class DestinoViaje {

    private selected: boolean;
    public servicios: string[];
    id = uuid();

    constructor(public nombre: string, public descripcion: string, public u: string, public votes: number = 0){
        this.servicios = ['pileta','desayuno'];
    }

    isSelected():boolean{
        return this.selected;
    }

    setSelected(s: boolean){
        this.selected = s;
    }

    VoteUp() {
        this.votes++;
    }

    VoteDown() {
        this.votes--;
    }
}
